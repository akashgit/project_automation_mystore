package Tests;



import org.testng.Assert;
import org.testng.annotations.Test;



import pageObjects.AddToCartPage;
import pageObjects.IndexPage;
import pageObjects.SearchResultPage;
import testBase.DataProviders;
import testBase.TestBase;

public class AddToCartPageTest extends TestBase
{
	private IndexPage index;
	private SearchResultPage searchResultPage;
	private AddToCartPage addToCartPage;
	
	@Test(groups = {"Regression","Sanity"}, dataProvider = "getProduct", dataProviderClass = DataProviders.class)
	public void addToCartTest(String productName, String qty, String size) throws Throwable {
		
		index= new IndexPage();
		searchResultPage=index.searchProduct(productName);
		addToCartPage=searchResultPage.clickOnProduct();
		addToCartPage.enterQuantity(qty);
		addToCartPage.selectSize(size);
		addToCartPage.clickOnAddToCart();
		boolean result=addToCartPage.validateAddtoCart();
		Assert.assertTrue(result);
		
		
	}

}
