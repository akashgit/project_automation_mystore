package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import pageObjects.IndexPage;
import testBase.TestBase;

public class IndexPageTest extends TestBase
{
	private IndexPage indexPage;
	
	@Test(groups = "Smoke")
	public void verifyLogo() throws Throwable {
		
		indexPage= new IndexPage();
		boolean result=indexPage.validateLogo();
		Assert.assertTrue(result);
		
	}
	
	@Test(groups = "Smoke")
	public void verifyTitle() {
		
		String actTitle=indexPage.getMyStoreTitle();
		Assert.assertEquals(actTitle, "My Store1");
		
	}

}
