package Tests;



import org.testng.Assert;
import org.testng.annotations.Test;



import pageObjects.HomePage;
import pageObjects.IndexPage;
import pageObjects.LoginPage;
import testBase.DataProviders;
import testBase.TestBase;

public class LoginPageTest extends TestBase
{
	private IndexPage indexPage;
	private LoginPage loginPage;
	private HomePage homePage;

	
	
	@Test(groups = {"Smoke","Sanity"},dataProvider = "credentials", dataProviderClass = DataProviders.class)
	public void loginTest(String uname, String pswd) throws Throwable 
	{
		
		indexPage= new IndexPage();
		
		loginPage=indexPage.clickOnSignIn();
		
	    //homePage=loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		homePage=loginPage.login(uname,pswd,homePage);
	    String actualURL=homePage.getCurrURL();
	    String expectedURL="http://automationpractice.com/index.php?controller=my-account";
	   
	    Assert.assertEquals(actualURL, expectedURL);
	}
	  
}
