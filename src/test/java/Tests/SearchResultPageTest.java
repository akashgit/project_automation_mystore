package Tests;



import org.testng.Assert;
import org.testng.annotations.Test;



import pageObjects.IndexPage;
import pageObjects.SearchResultPage;
import testBase.DataProviders;
import testBase.TestBase;

public class SearchResultPageTest extends TestBase
{
	private IndexPage index;
	private SearchResultPage searchResultPage;
	
	@Test(groups = "Smoke",dataProvider = "searchProduct", dataProviderClass = DataProviders.class)
	public void productAvailabilityTest(String productName) throws Throwable
	{
		
		index= new IndexPage();
		searchResultPage=index.searchProduct(productName);
		boolean result=searchResultPage.isProductAvailable();
		Assert.assertTrue(result);
		
	}


}
