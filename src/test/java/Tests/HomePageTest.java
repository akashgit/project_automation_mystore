package Tests;



import org.testng.Assert;
import org.testng.annotations.Test;



import pageObjects.HomePage;
import pageObjects.IndexPage;
import pageObjects.LoginPage;
import testBase.DataProviders;
import testBase.TestBase;

public class HomePageTest extends TestBase
{
	private IndexPage indexPage;
	private LoginPage loginPage;
	private HomePage homePage;

	
	@Test(groups = "Smoke",dataProviderClass = DataProviders.class)  
	public void wishListTest(String uname, String pswd) throws Throwable 
	{
		
		indexPage= new IndexPage();
		loginPage=indexPage.clickOnSignIn();
		homePage=loginPage.login(uname,pswd,homePage);
		boolean result=homePage.validateMyWishList();
		Assert.assertTrue(result);
		
	}
	
	@Test(groups = "Smoke",dataProvider = "credentials",dataProviderClass = DataProviders.class)  
	public void orderHistoryandDetailsTest(String uname, String pswd) throws Throwable 
	{
		
		indexPage= new IndexPage();
		loginPage=indexPage.clickOnSignIn();
		homePage=loginPage.login(uname,pswd,homePage);
		boolean result=homePage.validateOrderHistory();
		Assert.assertTrue(result);
		
	}
}
