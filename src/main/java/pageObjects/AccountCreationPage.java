package pageObjects;

import org.openqa.selenium.By;

import testBase.DriverFactory;
import testBase.TestBase;

public class AccountCreationPage extends TestBase
{
	
	By formTitle=By.xpath("//h1[text()='Create an account']");
	By mr=By.id("uniform-id_gender1");
	By mrs=By.id("id_gender2");
	By firstName=By.name("customer_firstname");
	By lastName=By.name("customer_lastname");
	By password=By.name("passwd");
	By days=By.name("days");
	By months=By.name("months");
	By years=By.name("years");
	By customerFirstName=By.name("firstname");
	By customerLastName=By.name("lastname");
	By companyName=By.name("company");
	By address=By.name("address1");
	By city=By.name("city");
	By state=By.name("id_state");
	By postcode=By.name("postcode");
	By country=By.name("id_country");
	By phone=By.name("phone");
	By mobile=By.name("phone_mobile");
	By ref=By.name("alias");
	By registerBtn=By.name("submitAccount");

	
	public void createAccount(String gender,
			String fName, 
			String lName, 
			String pswd, 
			String day, 
			String month, 
			String year,
			String comPany, 
			String addr, 
			String cityString, 
			String stateName, 
			String zip, 
			String countryName,
			String mobilePhone) throws Throwable
	{
		if(gender.equalsIgnoreCase("Mr"))
		{
			click_custom(DriverFactory.getInstance().getDriver().findElement(mr), "mr.");
		}
		else 
		{
			click_custom(DriverFactory.getInstance().getDriver().findElement(mrs), "mrs.");
		}
		
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(firstName), "First Name", fName);
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(lastName), "LastName", lName);
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(password), "Password", pswd);
		
		selectDropDownByValue_custom(DriverFactory.getInstance().getDriver().findElement(days), "Day", day);
		selectDropDownByValue_custom(DriverFactory.getInstance().getDriver().findElement(months), "Month", month);
		selectDropDownByValue_custom(DriverFactory.getInstance().getDriver().findElement(years), "Years", year);
		
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(companyName), "Company Name", comPany);
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(address), "Address", addr);
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(city), "City", cityString);
		
		selectDropDownByVisibleText_custom(DriverFactory.getInstance().getDriver().findElement(state), "State", stateName);
		
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(postcode), "Pin Code", zip);
		
		selectDropDownByVisibleText_custom(DriverFactory.getInstance().getDriver().findElement(country), "Country", countryName);
		
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(mobile), "Phone Number", mobilePhone);
		
	}
	
	
	
	
	public HomePage validateRegistration()
	{
		click_custom(DriverFactory.getInstance().getDriver().findElement(registerBtn), "Register Button");
		return new HomePage();
	}
	
	public boolean validateAcountCreatePage()
	{
		return isElementPresent_custom(DriverFactory.getInstance().getDriver().findElement(formTitle), "Register Page Title");
	}




}
