package pageObjects;

import org.openqa.selenium.By;

import testBase.DriverFactory;
import testBase.TestBase;

public class AddressPage extends TestBase
{
	
	By proceedToCheckOut=By.xpath("//span[text()='Proceed to checkout']");
	
	public ShippingPage clickOnCheckOut()
	{
		click_custom(DriverFactory.getInstance().getDriver().findElement(proceedToCheckOut), "proceedToCheckOut");
		return new ShippingPage();
	}
	

}
