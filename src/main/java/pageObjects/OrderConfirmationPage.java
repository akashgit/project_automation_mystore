package pageObjects;

import org.openqa.selenium.By;

import testBase.DriverFactory;
import testBase.TestBase;

public class OrderConfirmationPage extends TestBase
{
	By confirmMessag=By.xpath("//p/strong[contains(text(),'Your order on My Store is complete.')]");
	
	public String validateConfirmMessage()
	{
		String confirmMsg=getText_custom(DriverFactory.getInstance().getDriver().findElement(confirmMessag), "ConfirmMessag");
		return confirmMsg;
	}

}
