package pageObjects;

import org.openqa.selenium.By;




import testBase.DriverFactory;
import testBase.TestBase;

public class AddToCartPage extends TestBase
{
	
	By quantity=By.id("quantity_wanted");
	By size=By.name("group_1");
	By addToCartBtn=By.xpath("//span[text()='Add to cart']");
	By addToCartMessag=By.xpath("//*[@id=\"layer_cart\"]//h2/i");
	By proceedToCheckOutBtn=By.xpath("//span[contains(text(),'Proceed to checkout')]");
	
	public void enterQuantity(String quantity1)
	{
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(quantity), "quantity", quantity1);
	}
	
	
	public void selectSize(String size1) throws Throwable
	{
		selectDropDownByVisibleText_custom(DriverFactory.getInstance().getDriver().findElement(size), "size", size1);
	}

	
	
	public void clickOnAddToCart()
	{
		click_custom(DriverFactory.getInstance().getDriver().findElement(addToCartBtn), "addToCartBtn");
	}
	
	public boolean validateAddtoCart()
	{
		fluentWait_custom(DriverFactory.getInstance().getDriver().findElement(addToCartMessag), 30);
		return isElementPresent_custom(DriverFactory.getInstance().getDriver().findElement(addToCartMessag), "AddToCartMessage");
	}
	
	
	public OrderPage clickOnCheckOut()
	{
		fluentWait_custom(DriverFactory.getInstance().getDriver().findElement(proceedToCheckOutBtn), 10);
		jsClick_custom(DriverFactory.getInstance().getDriver().findElement(proceedToCheckOutBtn), "proceedToCheckOutBtn");
		return new OrderPage();
	}
	
	

}
