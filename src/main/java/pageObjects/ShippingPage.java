package pageObjects;

import org.openqa.selenium.By;

import testBase.DriverFactory;
import testBase.TestBase;

public class ShippingPage extends TestBase
{
	By terms=By.id("cgv");
	By proceedToCheckOutBtn=By.xpath("//button/span[contains(text(),'Proceed to checkout')]");
	
	
	public void checkTheTerms()
	{
		click_custom(DriverFactory.getInstance().getDriver().findElement(terms), "terms");
	}
	
	public PaymentPage clickOnProceedToCheckOut()
	{
		click_custom(DriverFactory.getInstance().getDriver().findElement(proceedToCheckOutBtn), "ProceedToCheckOutButton");
		return new PaymentPage();
	}

}
