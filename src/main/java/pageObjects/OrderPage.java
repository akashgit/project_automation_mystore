package pageObjects;

import org.openqa.selenium.By;



import testBase.DriverFactory;
import testBase.TestBase;

public class OrderPage extends TestBase
{
	
	By unitPrice=By.xpath("//td[@class='cart_unit']/span/span");
	By totalPrice=By.id("total_price");
	By proceedToCheckOut=By.xpath("//span[text()='Proceed to checkout']");
	
	public double getUnitPrice()
	{
		String unitPrice1=getText_custom(DriverFactory.getInstance().getDriver().findElement(unitPrice), "unitPrice");
		String unit=unitPrice1.replaceAll("[^a-zA-Z0-9]", "");
		double finalUnitPrice=Double.parseDouble(unit);
		return finalUnitPrice/100;
	}

	public double getTotalPrice()
	{
		String totalPrice1=getText_custom(DriverFactory.getInstance().getDriver().findElement(totalPrice), "TotalPrice");
		String tot=totalPrice1.replaceAll("[^a-zA-Z0-9]","");
		double finalTotalPrice=Double.parseDouble(tot);
		return finalTotalPrice/100;
	}
	
	
	public LoginPage clickOnCheckOut()
	{
		click_custom(DriverFactory.getInstance().getDriver().findElement(proceedToCheckOut), "proceedToCheckOut");
		return new LoginPage();
	}
	
	

	
	

}
