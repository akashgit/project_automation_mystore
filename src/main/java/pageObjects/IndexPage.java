package pageObjects;

import org.openqa.selenium.By;



import testBase.DriverFactory;
import testBase.TestBase;

public class IndexPage extends TestBase
{
	
	By signInBtn=By.xpath("//a[@class='login']");
	By myStoreLogo=By.xpath("//img[@class='logo img-responsive']");
	By searchProductBox=By.id("search_query_top");
	By searchButton=By.name("submit_search");
	
	
	
	public LoginPage clickOnSignIn()
	{
		fluentWait_custom(DriverFactory.getInstance().getDriver().findElement(signInBtn), 30);
		click_custom(DriverFactory.getInstance().getDriver().findElement(signInBtn), "SigninButton");
		return new LoginPage();
	}
	
	public boolean validateLogo()
	{
		return isElementPresent_custom(DriverFactory.getInstance().getDriver().findElement(myStoreLogo), "MyStoreLogo");
	}
	
	
	
	public String getMyStoreTitle()
	{
		String myStoretitl=DriverFactory.getInstance().getDriver().getTitle();
		return myStoretitl;
	}
	
	public SearchResultPage searchProduct(String productName) throws InterruptedException
	{
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(searchProductBox), "searchProductBox", productName);
		click_custom(DriverFactory.getInstance().getDriver().findElement(searchButton), "SearchButton");
		Thread.sleep(3000);
		return new SearchResultPage();
	}
	
	
	
	
	
	
	
	
	
}
