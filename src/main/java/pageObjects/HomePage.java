package pageObjects;

import org.openqa.selenium.By;


import testBase.DriverFactory;
import testBase.TestBase;

public class HomePage extends TestBase
{
	By mywish=By.xpath("//span[text()='My wishlists']");
	By orderHistory=By.xpath("//span[text()='Order history and details']");
	
	
	
	public boolean validateMyWishList()
	{
		return isElementPresent_custom(DriverFactory.getInstance().getDriver().findElement(mywish), "MyWishList");
	}
	
	public boolean validateOrderHistory()
	{
		return isElementPresent_custom(DriverFactory.getInstance().getDriver().findElement(orderHistory), "OrderHistory");
	}
	
	public String getCurrURL()
	{
		String hpurl=DriverFactory.getInstance().getDriver().getCurrentUrl();
		return hpurl;
	}
	
	
	
	
	
	
	
	

}
