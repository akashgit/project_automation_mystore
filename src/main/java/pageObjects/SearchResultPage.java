package pageObjects;

import org.openqa.selenium.By;

import testBase.DriverFactory;
import testBase.TestBase;

public class SearchResultPage extends TestBase
{
	By productResult=By.xpath("//*[@id=\"center_column\"]//img");
	
	
	
	
	public boolean isProductAvailable()
	{
		return isElementPresent_custom(DriverFactory.getInstance().getDriver().findElement(productResult), "productResult");
	}
	
	
	
	public AddToCartPage clickOnProduct()
	{
		click_custom(DriverFactory.getInstance().getDriver().findElement(productResult), "productResult click");
		return new AddToCartPage();
	}

}
