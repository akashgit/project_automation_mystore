package pageObjects;

import org.openqa.selenium.By;

import testBase.DriverFactory;
import testBase.TestBase;

public class PaymentPage extends TestBase
{
	By bankWireMethod=By.xpath("//a[contains(text(),'Pay by bank wire')]");
	By payByCheckMethod=By.xpath("//a[contains(text(),'Pay by check')]");
	
	public OrderSummary clickOnPaymentMethod()
	{
		click_custom(DriverFactory.getInstance().getDriver().findElement(bankWireMethod), "BankWireMethod");
		return new OrderSummary();
	}

}
