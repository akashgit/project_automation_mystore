package pageObjects;

import org.openqa.selenium.By;

import testBase.DriverFactory;
import testBase.TestBase;

public class LoginPage extends TestBase
{
	
	
	By EMAIL=By.id("email");
	By PASSWORD = By.id("passwd");
	By LOGIN_BTN = By.id("SubmitLogin");
	
	By EMAIL_CREATE=By.name("email_create");
	By CREATE_ACCOUNT_BTN=By.name("SubmitCreate");
	
	public HomePage login(String uname, String pswd,HomePage homePage) throws InterruptedException 
	{
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(EMAIL), "LoginEmailFIeld", uname);
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(PASSWORD), "LoginPasswordFIeld", pswd);

		click_custom(DriverFactory.getInstance().getDriver().findElement(LOGIN_BTN), "LoginButton");
		Thread.sleep(2000);
		homePage=new HomePage();
		return homePage;
	}
	
	public AddressPage login1(String uname, String pswd,AddressPage addressPage) throws InterruptedException 
	{
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(EMAIL), "LoginEmailFIeld", uname);
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(PASSWORD), "LoginPasswordFIeld", pswd);

		click_custom(DriverFactory.getInstance().getDriver().findElement(LOGIN_BTN), "LoginButton");
		Thread.sleep(2000);
		addressPage=new AddressPage();
		return addressPage;
	}

	public AccountCreationPage createNewAccount(String newEmail) throws Throwable 
	{
		sendKeys_custom(DriverFactory.getInstance().getDriver().findElement(EMAIL_CREATE), "EmailCreateField", newEmail);
		click_custom(DriverFactory.getInstance().getDriver().findElement(CREATE_ACCOUNT_BTN), "NewAccountCreate");
		return new AccountCreationPage();
	}
	
	
	
	

}
